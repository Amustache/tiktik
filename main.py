from telegram.ext import CommandHandler, MessageHandler, Updater, Filters
from io import BytesIO
import math
import os 
import io

import brotherlabel

from PIL import Image, ImageFont

from label import Label
import items

import qrcode

token = "2146073118:AAGsq_zygVmP2MrjHZiVJVgmtpmEVFTCsn8"
updater = Updater(token=token, use_context=True)

# Setup printer and create class MyLabel
backend = brotherlabel.USBBackend("usb://0x04f9:0x2085")
printer = brotherlabel.PTPrinter(backend)
printer.quality = brotherlabel.Quality.high_quality
tapetxt = '12'
printer.tape = brotherlabel.Tape.TZe12mm
printer.margin = 0
ratio = 1 #[0.2;1]
border = False
position = 'center'

fontsize = 120
fonttxt = "fonts/agepoly.ttf"
font = ImageFont.truetype(fonttxt, fontsize)


class MyLabel(Label):
        items = [
            [items.Text(font, pad_right=5, pad_left=5)]
        ]

height = printer.tape.value['print_area']


def main():
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('qr', make_qr))
    dispatcher.add_handler(CommandHandler('print', print_text))
    dispatcher.add_handler(CommandHandler('display', display_text))
    #dispatcher.add_handler(MessageHandler(Filters.photo, receive_images))
    dispatcher.add_handler(MessageHandler(Filters.document.mime_type("application/pdf"), printPDF))

    dispatcher.add_handler(CommandHandler('font', setFont))
    dispatcher.add_handler(CommandHandler('tape', setTape))
    dispatcher.add_handler(CommandHandler('ratio', setRatio))
    dispatcher.add_handler(CommandHandler('border', setBorder))
    dispatcher.add_handler(CommandHandler('position', setPosition))
    dispatcher.add_handler(CommandHandler('config', showConfig))
    dispatcher.add_handler(CommandHandler('status', showStatus))


    updater.start_polling()

def printPDF(update, context):
    print("Hello")
    context.bot.get_file(update.message.document).download(custom_path='outTemp.pdf')
    os.system("/usr/bin/lpr -P ageplp2_epfl_ch outTemp.pdf")
    os.remove("outTemp.pdf")

def showStatus(update, context):
    print(printer.get_status().to_string())


def setFont(update, context):
    global font
    global fonttxt
    global MyLabel
    message = ' '.join(context.args)

    if message == 'arial':
        fonttxt = "fonts/arialbi.ttf"
    elif message == 'roboto':
        fonttxt = "fonts/roboto.ttf"
    elif message == 'dancing':
        fonttxt = "fonts/dancing.ttf"
    elif message == 'agepoly':
        fonttxt = "fonts/agepoly.ttf"
    else:
        fonttxt = "fonts/arialbi.ttf"

    font = ImageFont.truetype(fonttxt, fontsize)
    MyLabel.items = [
            [items.Text(font, pad_right=5, pad_left=5)]
        ]
    context.bot.sendMessage(update.effective_chat.id, "Police défini sur: "+fonttxt[2:-4])

def setRatio(update, context):
    global ratio

    message = float(' '.join(context.args))
    if message < 0.2:
        ratio = 0.2
    elif message > 1:
        ratio = 1
    else:
        ratio = message
    context.bot.sendMessage(update.effective_chat.id, 'Ratio défini sur: '+str(ratio))

def setBorder(update, context):
    global border

    message = ' '.join(context.args)
    message = message.lower()
    if message == 'oui' or message == 'yes' or message == 'true' or message == 'ui' or message == '1':
        border = True
    elif message == 'non' or message == 'no' or message == 'false' or message == 'nope' or message == '0':
        border = False

    context.bot.sendMessage(update.effective_chat.id, 'Bordure défini sur: '+str(border))

def setPosition(update, context):
    global position

    message = ' '.join(context.args)
    message = message.lower()
    if message == 'top' or message == 'haut':
        position = 'top'
    elif message == 'center' or message == 'centre':
        position = 'center'
    elif message == 'bottom' or message == 'bas':
        position = 'bottom'

    context.bot.sendMessage(update.effective_chat.id, 'Position défini sur: '+ position)


def setTape(update, context):
    global tapetxt
    global printer
    global height
    tapetxt = ' '.join(context.args)

    if tapetxt == '36':
        printer.tape = brotherlabel.Tape.TZe36mm
        context.bot.sendMessage(update.effective_chat.id, "Tape défini sur: 36mm")
    elif tapetxt == '24':
        printer.tape = brotherlabel.Tape.TZe24mm
        context.bot.sendMessage(update.effective_chat.id, "Tape défini sur: 24mm")
    elif tapetxt == '18':
        printer.tape = brotherlabel.Tape.TZe18mm
        context.bot.sendMessage(update.effective_chat.id, "Tape défini sur: 18mm")
    elif tapetxt == '12':
        printer.tape = brotherlabel.Tape.TZe12mm
        context.bot.sendMessage(update.effective_chat.id, "Tape défini sur: 12mm")
    elif tapetxt == '9':
        printer.tape = brotherlabel.Tape.TZe9mm
        context.bot.sendMessage(update.effective_chat.id, "Tape défini sur: 9mm")
    elif tapetxt == '6':
        printer.tape = brotherlabel.Tape.TZe6mm
        context.bot.sendMessage(update.effective_chat.id, "Tape défini sur: 6mm")
    elif tapetxt == '3.5':
        printer.tape = brotherlabel.Tape.TZe3_5mm
        context.bot.sendMessage(update.effective_chat.id, "Tape défini sur: 3.5mm")

    height = printer.tape.value['print_area']

def showConfig(update, context):
    context.bot.sendMessage(update.effective_chat.id, "Police: "+fonttxt[2:-4]+ "\nTape: "+tapetxt + "\nRatio: "+str(ratio) + "\nBordure: "+str(border) + "\nPosition: "+position)


def display_text(update, context):
    if(len(context.args) == 0):
        context.bot.sendMessage(update.effective_chat.id, "Donne moi le texte que tu veux imprimer stp!")
        
    text = ' '.join(context.args)

    img = MyLabel(text).render(ratio=ratio, border=border, start=position, height=height)
    
    output = io.BytesIO()
    img.save(output, format="png")
    img_str= output.getvalue()

    #Send message that it's printing
    context.bot.sendMessage(update.effective_chat.id, "J'imprime: " + text)
    context.bot.sendPhoto(update.effective_chat.id, img_str)
    

def print_text(update, context):
    global backend
    global printer
    try:
        printer.get_status().to_string()
    except:
        backend = brotherlabel.USBBackend("usb://0x04f9:0x2085")
        printer = brotherlabel.PTPrinter(backend)
        printer.quality = brotherlabel.Quality.high_quality
        if tapetxt == '36':
            printer.tape = brotherlabel.Tape.TZe36mm
        elif tapetxt == '24':
            printer.tape = brotherlabel.Tape.TZe24mm
        elif tapetxt == '18':
            printer.tape = brotherlabel.Tape.TZe18mm
        elif tapetxt == '12':
            printer.tape = brotherlabel.Tape.TZe12mm
        elif tapetxt == '9':
            printer.tape = brotherlabel.Tape.TZe9mm
        elif tapetxt == '6':
            printer.tape = brotherlabel.Tape.TZe6mm
        elif tapetxt == '3.5':
            printer.tape = brotherlabel.Tape.TZe3_5mm
        printer.margin = 0

    if(len(context.args) == 0):
        context.bot.sendMessage(update.effective_chat.id, "Donne moi le texte que tu veux imprimer stp!")
        
    text = ' '.join(context.args)

    img = MyLabel(text).render(ratio=ratio, border=border, start=position, height=height)

    #Send message that it's printing
    context.bot.sendMessage(update.effective_chat.id, "J'imprime: " + text)

    printer.print([img])

def make_qr(update, context):
    if(len(context.args) == 0):
        context.bot.sendMessage(update.effective_chat.id, "Donne moi le texte que tu veux insérer dans un QRCode stp !")

    text = ' '.join(context.args)

    #Generate qrcode and resize
    qrcode = qrcode.make(text)
    img = qrcode.resize((454, 454))

    #Send message that it's printing
    context.bot.sendMessage(update.effective_chat.id, "J'imprime le QRCode: " + text)

    #printer.print([img])
    
def receive_images(update, context):
    #Download the image and double the size
    downloaded_img = Image.open(BytesIO(context.bot.getFile(update.message.photo[-1].file_id).download_as_bytearray()))
    decode_img = downloaded_img.resize((downloaded_img.width*2, downloaded_img.height*2))

    #Confirm printing, split image by height strips and print
    context.bot.sendMessage(update.effective_chat.id, "Preparing scissors and glue...")
    for i in range(0, math.ceil(decode_img.height/height)):
        printer.print([decode_img.crop((0, height*i, decode_img.width, min(height*(i+1), decode_img.height))).convert('1')])

def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="""Hey! Bienvenue sur le bot TikTik ! Si tu as besoin de plus d'informations: https://wiki2.agepoly.ch/e/fr/comite-de-direction/logistique/tiktik-bot""")

if __name__ == '__main__':
    main()