"""
Labels are the Base class you derive your Labels from. A few simple Labels are
provided for you.
"""
from typing import Tuple

from PIL import Image, ImageChops, ImageDraw


def _coord_add(tup1, tup2):
    """add two tuples of size two"""
    return (tup1[0] + tup2[0], tup1[1] + tup2[1])


class Label:
    """Base class for all labels

    >>> class MyLabel(Label):
    ...     items = [
    ...         Text(), Text()
    ...     ]
    >>> l = MyLabel("text1", "text2")
    >>> printer.print(l)

    """
    items = []  # type: list

    def __init__(self, *args):
        if not self.items:
            raise ValueError(
                "A Labels 'items' attribute must contain a list of "
                "renderable objects")

        arg_it = iter(args)
        try:
            self._rendered_items = [
                [item.render(next(arg_it)) for item in line]
                for line in self.items]
        except StopIteration:
            # the argument list was exhausted before all items had a value
            raise TypeError("{cls} requires {argc} arguments, but {num} were given".format(
                cls=self.__class__.__name__, argc=sum(len(x) for x in self.items), num=len(args)
            ))

    @property
    def size(self) -> Tuple[int, int]:
        width = max(sum(i.size[0] for i in line)
                    for line in self._rendered_items)
        height = sum(max(i.size[1] for i in line)
                     for line in self._rendered_items)

        return width, height

    def render(self, ratio=1, border=False, start='center', width=None, height=None) -> Image:
        """render the Label.

        Args:
            width: Width request
            height: Height request
        """
        size = self.size
        size = (round(size[0]*ratio), size[1])
        img = Image.new("1", size, "white")

        pos = [0, 0]
        shape = [(2, 2), (size[0] - 2, size[1] - 2)]
        i = 0

        posStart = 0
        xStart = 0

        if start == 'center':
            posStart = round(size[1]*((1-ratio)/2))
        elif start == 'top':
            posStart = 0
            xStart = 0
            if ratio<=0.9:
                posStart = posStart + 4
                xStart = -round(size[1]*((1-ratio)/2)) + 4
        elif start == 'bottom':
            posStart = round((size[1]- size[1]*ratio ))
            if ratio<=0.9:
                posStart = posStart - 4
                xStart = round(size[1]*((1-ratio)/2)) - 4


        for line in self._rendered_items:
            for item in line:
                box = (*pos, *_coord_add(item.size, pos))
                itemWidth, itemHeight = item.size
                item = item.resize(((round(itemWidth*ratio), round(itemHeight*ratio))))

                img.paste(item, (0, posStart)) 
                pos[0] += item.size[0]

            pos[0] = 0
            pos[1] += max(i.size[1] for i in line)

        
        if border == 1 and ratio <= 0.9:
            draw =  ImageDraw.Draw(img)
            
            diffX = 2 - (size[1]*ratio - size[1]*0.9)/2
            diffY = 2
            radius = 10 - 9*(0.9-ratio)

            
            #top
            draw.line([(diffY+ radius,diffX + xStart), (size[0] - diffY - radius, diffX + xStart)], fill ="black", width = 1)
            #right
            draw.line([(size[0] - diffY,diffX + xStart + radius), (size[0] - diffY, size[1] - diffX - radius + xStart)], fill ="black", width = 1)
            #bottom
            draw.line([(diffY+ radius,size[1] + xStart - diffX), (size[0] - diffY -radius, size[1] - diffX + xStart)], fill ="black", width = 1)
            #left
            draw.line([(diffY,diffX + xStart + radius), (diffY, size[1] - diffX + xStart - radius)], fill ="black", width = 1)

            #top left
            draw.arc([(diffY, diffX + xStart), (diffY + 2*radius, diffX + xStart + 2*radius)], 180, 270, fill="black", width=1)
            #bottom left
            draw.arc([(diffY, size[1] - diffX - 2*radius + xStart), (diffY + 2*radius, size[1] - diffX + xStart)], 90, 180, fill="black", width=1)
            #top right
            draw.arc([(size[0] - diffY - 2*radius, diffX + xStart), (size[0] - diffY, diffX+ 2*radius + xStart)], 270, 359, fill="black", width=1)
            #bottom right
            draw.arc([(size[0] - diffY - 2*radius, size[1] - diffX - 2*radius + xStart), (size[0] - diffY, size[1] - diffX + xStart)], 0, 90, fill="black", width=1)

        xdim, ydim = img.size
        xdim = xdim
        ydim = ydim

        height = round(height)
        xdim = round((height / ydim) * xdim)

        img = img.resize((xdim, height))

        # if not img.mode == "1":
        #     raise ValueError("render output has invalid mode '1'")
        # img = img.transpose(Image.ROTATE_270).transpose(
        #     Image.FLIP_TOP_BOTTOM)
        # img = ImageChops.invert(img)
        
        return img
# print("".join(f"{x:08b}".replace("0", " ") for x in bytes(i)))
